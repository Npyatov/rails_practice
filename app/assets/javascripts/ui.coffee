(($) ->
	file_api = true if window.File and window.FileReader and window.FileList and window.Blob
	
	file_field = $('.file-field__input')
	console.log(file_field)
	file_field.on 'change', (event) =>
		file_name = "unknown"

		if file_api and file_field[0].files[0] 
			file_name = file_field[0].files[0].name
		else
			file_name = "No file chosen"

		$('.file-field__file-name').text(file_name)
	.change()
) jQuery