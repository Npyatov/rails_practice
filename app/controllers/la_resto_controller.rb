class LaRestoController < ApplicationController
  def home
    @menu_parts = MenuPart.where(page: "main")
    @header_root = true
  end

  def about
  end

  def menu 
  	@deserts = MenuPart.find_by(name: "Deserts")
    @wine_list = MenuPart.find_by(name: "Wine List")
    @menu_parts = MenuPart.where(page: "menu")
  end

  def reservation
  	@reservation = Reservation.new
  end

  def news
  	@articles = Article.all
  end

  def blog
  	
  end

  def contacts
  	
  end
end
