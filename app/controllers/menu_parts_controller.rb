class MenuPartsController < ApplicationController
  before_action :set_menu_part, only: [:show, :edit, :update, :destroy]

  # GET /menu_parts
  # GET /menu_parts.json
  def index
    @menu_parts = MenuPart.all
  end

  # GET /menu_parts/1
  # GET /menu_parts/1.json
  def show
  end

  # GET /menu_parts/new
  def new
    @menu_part = MenuPart.new
  end

  # GET /menu_parts/1/edit
  def edit
  end

  # POST /menu_parts
  # POST /menu_parts.json
  def create
    @menu_part = MenuPart.new(menu_part_params)

    respond_to do |format|
      if @menu_part.save
        format.html { redirect_to @menu_part, notice: 'Menu part was successfully created.' }
        format.json { render :show, status: :created, location: @menu_part }
      else
        format.html { render :new }
        format.json { render json: @menu_part.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /menu_parts/1
  # PATCH/PUT /menu_parts/1.json
  def update
    respond_to do |format|
      if @menu_part.update(menu_part_params)
        format.html { redirect_to @menu_part, notice: 'Menu part was successfully updated.' }
        format.json { render :show, status: :ok, location: @menu_part }
      else
        format.html { render :edit }
        format.json { render json: @menu_part.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /menu_parts/1
  # DELETE /menu_parts/1.json
  def destroy
    @menu_part.destroy
    respond_to do |format|
      format.html { redirect_to menu_parts_url, notice: 'Menu part was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def show_image
    @menu_part = MenuPart.find(params[:id])
    send_data @menu_part.img, type: 'image/jpeg', disposition: 'attachment'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_menu_part
      @menu_part = MenuPart.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def menu_part_params
      params.require(:menu_part).permit(:name, :img)
    end
end
