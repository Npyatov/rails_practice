module LaRestoHelper
	
	def separator_block(size = 12, modificator = false)
		cl = "separator col-sm-#{size} "
		cl += "separator_inverse" if modificator == true
		separator = content_tag(:div, "" , class: cl )
		content_tag(:div, 
			content_tag(:div, separator , class: 'row'), class: 'container-fluid') 
	end

end
