class MenuItem < ApplicationRecord
	has_one :payment
	belongs_to :menu_part
	has_attached_file :img, styles: { medium: "300x300>", thumb: "100x100>"}, default_url: "/images/:style/missing.png"
	validates_attachment_content_type :img, content_type: /\Aimage\/.*\z/	
	validates :name, presence: true, length: { maximum: 50 }
	validates :cost, presence: true, numericality: { greater_than: 0 }
end
