class MenuPart < ApplicationRecord
	has_many :menu_items
	has_attached_file :img, styles: { medium: "500x500>", thumb: "100x100>"}, default_url: "/images/:style/missing.png"
	validates_attachment_content_type :img, content_type: /\Aimage\/.*\z/	
end
