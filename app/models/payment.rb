class Payment < ApplicationRecord
	belongs_to :user
	has_many :menu_items

	serialize :notification_params, Hash
	def paypal_url(return_url)
		values = {
			cmd: '_xclick',
			charset: 'utf-8',
			business: 'ghostlinee-facilitator@gmail.com',
			return: "#{Rails.application.secrets.app_host}#{return_url}",
			invoice: id,
			item_number: 2,
			item_name: 'name',
			currency_code: 'RUB', 
			amount: 100,
			notify_url: "#{Rails.application.secrets.app_host}/hook"
		}
		"#{Rails.application.secrets.paypal_host}/cgi-bin/websrc?#{values.to_query}"
	end
end
