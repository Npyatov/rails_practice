class Post < ApplicationRecord
	has_attached_file :img, styles: { medium: "500x500>", thumb: "100x100>"}, default_url: "/images/:style/missing.png"
end
