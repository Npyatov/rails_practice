json.extract! menu_part, :id, :name, :created_at, :updated_at
json.url menu_part_url(menu_part, format: :json)