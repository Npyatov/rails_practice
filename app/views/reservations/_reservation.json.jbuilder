json.extract! reservation, :id, :name, :email, :phone, :comment, :created_at, :updated_at
json.url reservation_url(reservation, format: :json)