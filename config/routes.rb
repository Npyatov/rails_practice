Rails.application.routes.draw do
  resources :posts
  resources :articles
  resources :reservations
  resources :menu_parts
  match '/payments/:id',         to: 'payments#show', via: 'post'
  match '/hook',                 to: 'payments#hook', via: 'post' 
  resources :payments
  resources :menu_items
  resources :users
  resources :sessions, only: [:new, :create, :destroy]
  match 'la-resto/',            to: 'la_resto#home', via: 'get' 
  match 'la-resto/about',       to: 'la_resto#about', via: 'get'
  match 'la-resto/menu',        to: 'la_resto#menu', via: 'get'
  match 'la-resto/reservation', to: 'la_resto#reservation', via: 'get'
  match 'la-resto/news',        to: 'la_resto#news', via: 'get'
  match 'la-resto/blog',        to: 'la_resto#blog', via: 'get'
  match 'la-resto/contacts',    to: 'la_resto#contacts', via: 'get'
  match 'signup',				to: 'users#new', via: 'get'
  match '/login', 				to: 'sessions#new', via: 'get'
  match '/logout',				to: 'sessions#destroy', via: 'delete' 
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
