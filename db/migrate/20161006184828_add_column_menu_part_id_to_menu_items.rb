class AddColumnMenuPartIdToMenuItems < ActiveRecord::Migration[5.0]
  def change
    add_column :menu_items, :menu_part_id, :integer
  end
end
