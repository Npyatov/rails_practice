class AddColumnImgToMenuParts < ActiveRecord::Migration[5.0]
  def change
    add_column :menu_parts, :img, :binary
  end
end
