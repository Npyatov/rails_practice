class RemoveImgFromMenuParts < ActiveRecord::Migration[5.0]
  def change
  	remove_column :menu_parts, :img
  end
end
