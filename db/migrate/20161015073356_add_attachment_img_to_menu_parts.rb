class AddAttachmentImgToMenuParts < ActiveRecord::Migration
  def self.up
    change_table :menu_parts do |t|
      t.attachment :img
    end
  end

  def self.down
    remove_attachment :menu_parts, :img
  end
end
