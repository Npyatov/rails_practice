class CreateArticles < ActiveRecord::Migration[5.0]
  def change
    create_table :articles do |t|
      t.string :name
      t.string :short_descrition
      t.text :descriotion
      t.attachment :img

      t.timestamps
    end
  end
end
