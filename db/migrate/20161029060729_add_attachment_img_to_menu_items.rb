class AddAttachmentImgToMenuItems < ActiveRecord::Migration
  def self.up
    change_table :menu_items do |t|
      t.attachment :img
    end
  end

  def self.down
    remove_attachment :menu_items, :img
  end
end
