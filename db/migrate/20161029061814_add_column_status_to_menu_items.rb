class AddColumnStatusToMenuItems < ActiveRecord::Migration[5.0]
  def change
    add_column :menu_items, :status, :string
  end
end
