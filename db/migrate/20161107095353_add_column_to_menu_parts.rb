class AddColumnToMenuParts < ActiveRecord::Migration[5.0]
  def change
    add_column :menu_parts, :page, :string
  end
end
