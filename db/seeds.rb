# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

def path_image(name)
	File.join(Rails.root, "app", "assets", "images", name)
end

starters = MenuPart.create(name: 'Starters', img: File.new(path_image("starters.jpg")), page: "main")
main_courses = MenuPart.create(name: 'Main Courses', img: File.new(path_image("main_courses.jpg")), page: "main")
drinks = MenuPart.create(name: 'Drinks', img: File.new(path_image("drinks.jpg")), page: "main")
wine_list = MenuPart.create(name: "Wine List")
deserts = MenuPart.create(name: 'Deserts', img: File.new(path_image("deserts.jpg")), page: "main")


deserts_items = MenuItem.create([{name: "Lemon meringue cheesecake", cost: 13.5, menu_part: deserts},
								 {name: "Orange polenta cake", cost: 6, menu_part: deserts},
								 {name: "Tiramisu", cost: 8, menu_part: deserts},
								 {name: "Chocolate praline pudding", cost: 7.2, menu_part: deserts}])
wine_list_items = MenuItem.create([{name: "Prendina, Pinot Grigio 2012", cost: 25.9, menu_part: wine_list},
								  {name: "Terlano, Nova Domus 2010", cost: 23.1, menu_part: wine_list},
								  {name: "Willian Hill, Chardonnay 2013", cost: 15.8, menu_part: wine_list},
								  {name: "Krug, Grand Cuvee", cost: 30.35, menu_part: wine_list},
								  {name: "Rose, Brut", cost: 70.8, menu_part: wine_list},
								  {name: "Dom Perignon 2004", cost: 29.4, menu_part: wine_list},
								  {name: "Louis de Sacy, Grand Cru 2006", cost: 24.99, menu_part: wine_list}])
MenuItem.create([{name: "Girandole ricotta e spinaci", cost: 14, status: "new", menu_part: main_courses,
					img: File.new(path_image("recommended_new.jpg"))},
				 {name: "Insalata di  mare", cost: 12, status: "chef's choice", menu_part: main_courses,
				 	img: File.new(path_image("recommended_chef_choice.jpg"))}])


MenuPart.create([{name: "Appetizers", img: File.new(path_image("our-menu1.jpg")), page: "menu"},
				 {name: "Sauces & Spaghetti", img: File.new(path_image("our-menu2.jpg")), page: "menu"},
				 {name: "Classic Lasagna", img: File.new(path_image("our-menu3.jpg")), page: "menu"},
				 {name: "Chiken BBQ", img: File.new(path_image("our-menu4.jpg")), page: "menu"},
				 {name: "Classic Pizza", img: File.new(path_image("our-menu5.jpg")), page: "menu"},
				 {name: "Soups & Salads", img: File.new(path_image("our-menu6.jpg")), page: "menu"},
				 {name: "Vegetable Pizza", img: File.new(path_image("our-menu7.jpg")), page: "menu"},
				 {name: "Pasta with Seafood", img: File.new(path_image("our-menu8.jpg")), page: "menu"}])


