require "rails_helper"

RSpec.describe UsersController, :type => :controller do
	describe "GET #index" do 
		it "assigns all users as @users" do
			users = User.all
			get :index
			expect(assigns(:users)).to eq(users)
		end
	end
end