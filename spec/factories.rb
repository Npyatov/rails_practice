FactoryGirl.define do 
	factory :user do
		name "Nik Pyatov"
		email "nik@mail.com"
		password "foobar"
		password_confirmation "foobar"
	end
end