require 'rails_helper'
require 'spec_helper'

RSpec.describe "MenuItems" do

	subject { page }

  describe "GET /menu_items/new" do
  	before { visit new_menu_item_path }
    it { should have_content('New Menu Item') }
    it { should have_title('MyApp | New Item') }
    it { should_not have_title('MyApp | Edit Item') }
  end
end

RSpec.describe MenuItem do
	before { @menu_item = MenuItem.new(name: "Pizza", cost: 450)  }

	subject { @menu_item }

	it { respond_to(:name) }
	it { respond_to(:cost) }

	it { should be_valid }

	describe "when name is not present" do
		before { @menu_item.name = " " }
		it { should_not be_valid }
	end

	describe "when name is too long" do
		before { @menu_item.name = "a" * 100 }
		it { should_not be_valid }
	end

	describe "when cost below zero" do 
		before { @menu_item.cost = -15}
		it { should_not be_valid }
	end
end
