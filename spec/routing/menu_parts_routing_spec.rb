require "rails_helper"

RSpec.describe MenuPartsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/menu_parts").to route_to("menu_parts#index")
    end

    it "routes to #new" do
      expect(:get => "/menu_parts/new").to route_to("menu_parts#new")
    end

    it "routes to #show" do
      expect(:get => "/menu_parts/1").to route_to("menu_parts#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/menu_parts/1/edit").to route_to("menu_parts#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/menu_parts").to route_to("menu_parts#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/menu_parts/1").to route_to("menu_parts#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/menu_parts/1").to route_to("menu_parts#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/menu_parts/1").to route_to("menu_parts#destroy", :id => "1")
    end

  end
end
