require 'rails_helper'

RSpec.describe "articles/edit", type: :view do
  before(:each) do
    @article = assign(:article, Article.create!(
      :name => "MyString",
      :short_descrition => "MyString",
      :descriotion => "MyText"
    ))
  end

  it "renders the edit article form" do
    render

    assert_select "form[action=?][method=?]", article_path(@article), "post" do

      assert_select "input#article_name[name=?]", "article[name]"

      assert_select "input#article_short_descrition[name=?]", "article[short_descrition]"

      assert_select "textarea#article_descriotion[name=?]", "article[descriotion]"
    end
  end
end
