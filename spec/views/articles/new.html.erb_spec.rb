require 'rails_helper'

RSpec.describe "articles/new", type: :view do
  before(:each) do
    assign(:article, Article.new(
      :name => "MyString",
      :short_descrition => "MyString",
      :descriotion => "MyText"
    ))
  end

  it "renders new article form" do
    render

    assert_select "form[action=?][method=?]", articles_path, "post" do

      assert_select "input#article_name[name=?]", "article[name]"

      assert_select "input#article_short_descrition[name=?]", "article[short_descrition]"

      assert_select "textarea#article_descriotion[name=?]", "article[descriotion]"
    end
  end
end
