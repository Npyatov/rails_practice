require 'rails_helper'

RSpec.describe "menu_parts/edit", type: :view do
  before(:each) do
    @menu_part = assign(:menu_part, MenuPart.create!(
      :name => "MyString"
    ))
  end

  it "renders the edit menu_part form" do
    render

    assert_select "form[action=?][method=?]", menu_part_path(@menu_part), "post" do

      assert_select "input#menu_part_name[name=?]", "menu_part[name]"
    end
  end
end
