require 'rails_helper'

RSpec.describe "menu_parts/index", type: :view do
  before(:each) do
    assign(:menu_parts, [
      MenuPart.create!(
        :name => "Name"
      ),
      MenuPart.create!(
        :name => "Name"
      )
    ])
  end

  it "renders a list of menu_parts" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
  end
end
