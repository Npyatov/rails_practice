require 'rails_helper'

RSpec.describe "menu_parts/new", type: :view do
  before(:each) do
    assign(:menu_part, MenuPart.new(
      :name => "MyString"
    ))
  end

  it "renders new menu_part form" do
    render

    assert_select "form[action=?][method=?]", menu_parts_path, "post" do

      assert_select "input#menu_part_name[name=?]", "menu_part[name]"
    end
  end
end
