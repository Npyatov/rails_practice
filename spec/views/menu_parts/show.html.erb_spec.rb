require 'rails_helper'

RSpec.describe "menu_parts/show", type: :view do
  before(:each) do
    @menu_part = assign(:menu_part, MenuPart.create!(
      :name => "Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
  end
end
