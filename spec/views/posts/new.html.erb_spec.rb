require 'rails_helper'

RSpec.describe "posts/new", type: :view do
  before(:each) do
    assign(:post, Post.new(
      :name => "MyString",
      :short_descrition => "MyString",
      :descriotion => "MyText"
    ))
  end

  it "renders new post form" do
    render

    assert_select "form[action=?][method=?]", posts_path, "post" do

      assert_select "input#post_name[name=?]", "post[name]"

      assert_select "input#post_short_descrition[name=?]", "post[short_descrition]"

      assert_select "textarea#post_descriotion[name=?]", "post[descriotion]"
    end
  end
end
