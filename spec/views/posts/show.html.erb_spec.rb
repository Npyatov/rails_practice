require 'rails_helper'

RSpec.describe "posts/show", type: :view do
  before(:each) do
    @post = assign(:post, Post.create!(
      :name => "Name",
      :short_descrition => "Short Descrition",
      :descriotion => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Short Descrition/)
    expect(rendered).to match(/MyText/)
  end
end
