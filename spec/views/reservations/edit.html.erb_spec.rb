require 'rails_helper'

RSpec.describe "reservations/edit", type: :view do
  before(:each) do
    @reservation = assign(:reservation, Reservation.create!(
      :name => "MyString",
      :email => "MyString",
      :phone => "MyString",
      :comment => "MyText"
    ))
  end

  it "renders the edit reservation form" do
    render

    assert_select "form[action=?][method=?]", reservation_path(@reservation), "post" do

      assert_select "input#reservation_name[name=?]", "reservation[name]"

      assert_select "input#reservation_email[name=?]", "reservation[email]"

      assert_select "input#reservation_phone[name=?]", "reservation[phone]"

      assert_select "textarea#reservation_comment[name=?]", "reservation[comment]"
    end
  end
end
